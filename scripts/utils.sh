#!/usr/bin/env bash

function configure() {
    export ROOT=$PWD
    export PYTHONPATH=$PYTHONPATH:$ROOT/lib/python

    echo "ROOT=$ROOT"
    echo "PYTHONPATH=$PYTHONPATH"

    echo "Done running configure"
}
