from dataclasses import dataclass
import sqlite3

DATABASE_URL = "notes.db"  # TODO: change to environment variable


@dataclass
class Note:
    id: int
    text: str


def get_db_connection(database_url: str = DATABASE_URL) -> sqlite3.Connection:
    return sqlite3.connect(database_url)


def db_create_table(db: sqlite3.Connection) -> None:
    cursor = db.cursor()
    cursor.execute(
        "CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY, text TEXT)"
    )
    db.commit()


def db_delete_table(db: sqlite3.Connection) -> None:
    cursor = db.cursor()
    cursor.execute("DROP TABLE IF EXISTS notes")
    db.commit()


def db_find_note(id: int, db: sqlite3.Connection) -> Note:
    cursor = db.cursor()
    cursor.execute(f"SELECT * FROM notes WHERE id={id}")
    result = cursor.fetchone()
    if result is None:
        raise Exception(f"Could not find note with id={id}")
    return Note(*result)


def db_create_note(text: str, db: sqlite3.Connection) -> Note:
    cursor = db.cursor()
    cursor.execute(f"INSERT INTO notes (text) VALUES ('{text}')")
    db.commit()
    id = cursor.lastrowid
    return Note(id, text)  # type: ignore


def db_read_note(id: int, db: sqlite3.Connection) -> Note:
    return db_find_note(id, db)


def db_read_notes(db: sqlite3.Connection) -> list[Note]:
    cursor = db.cursor()
    cursor.execute("SELECT * FROM notes")
    return [Note(*result) for result in cursor.fetchall()]


def db_update_note(id: int, text: str, db: sqlite3.Connection) -> Note:
    db_find_note(id, db)
    cursor = db.cursor()
    cursor.execute(f"UPDATE notes SET text='{text}' WHERE id={id}")
    db.commit()
    return Note(id, text)


def db_delete_note(id: int, db: sqlite3.Connection) -> Note:
    note = db_find_note(id, db)
    cursor = db.cursor()
    cursor.execute(f"DELETE FROM notes WHERE id={id}")
    db.commit()
    return note
