from .operations import (
    Note,
    db_create_note,
    db_delete_note,
    db_read_note,
    db_update_note,
    get_db_connection,
)


def create_note(text: str) -> Note:
    db = get_db_connection()
    try:
        return db_create_note(text, db)
    finally:
        db.close()


def read_note(id: int) -> Note:
    db = get_db_connection()
    try:
        return db_read_note(id, db)
    finally:
        db.close()


def read_notes() -> list[Note]:
    db = get_db_connection()
    try:
        return db_read_notes(db)
    finally:
        db.close()


def update_note(id: int, text: str) -> Note:
    db = get_db_connection()
    try:
        return db_update_note(id, text, db)
    finally:
        db.close()


def delete_note(id: int) -> None:
    db = get_db_connection()
    try:
        db_delete_note(id, db)
    finally:
        db.close()
