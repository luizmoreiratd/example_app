from typing import Generator
import pytest
import sqlite3

from notes.operations import (
    db_delete_table,
    get_db_connection,
    db_create_table,
    db_create_note,
    db_read_note,
    db_read_notes,
    db_update_note,
    db_delete_note,
)

DATABASE_URL = ":memory:"


@pytest.fixture
def test_db() -> Generator[sqlite3.Connection, None, None]:
    db = get_db_connection(DATABASE_URL)
    try:
        db_create_table(db)
        yield db
    finally:
        db_delete_table(db)
        db.close()


def test_create_note(test_db: sqlite3.Connection) -> None:
    note = db_create_note("test create note", test_db)
    assert note.id == 1
    assert note.text == "test create note"


def test_read_note(test_db: sqlite3.Connection) -> None:
    db_create_note("test read note", test_db)
    note = db_read_note(1, test_db)
    assert note.id == 1
    assert note.text == "test read note"


def test_read_notes_empty(test_db: sqlite3.Connection) -> None:
    assert db_read_notes(test_db) == []


def test_read_notes(test_db: sqlite3.Connection) -> None:
    note1 = db_create_note("test read note 1", test_db)
    note2 = db_create_note("test read note 2", test_db)

    notes = db_read_notes(test_db)
    assert len(notes) == 2
    assert notes[0] == note1
    assert notes[1] == note2


def test_read_note_not_found(test_db: sqlite3.Connection) -> None:
    with pytest.raises(Exception):
        db_read_note(1, test_db)


def test_update_note(test_db: sqlite3.Connection) -> None:
    db_create_note("test update note", test_db)
    note = db_update_note(1, "test updated note", test_db)
    assert note.id == 1
    assert note.text == "test updated note"


def test_update_note_not_fund(test_db: sqlite3.Connection) -> None:
    with pytest.raises(Exception):
        db_update_note(1, "test update note", test_db)


def test_delete_note(test_db: sqlite3.Connection) -> None:
    db_create_note("test delete note", test_db)
    note = db_delete_note(1, test_db)
    assert note.id == 1
    assert note.text == "test delete note"


def test_delete_note_not_found(test_db: sqlite3.Connection) -> None:
    with pytest.raises(Exception):
        db_delete_note(1, test_db)
